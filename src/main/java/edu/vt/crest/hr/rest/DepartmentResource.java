package edu.vt.crest.hr.rest;

import edu.vt.crest.hr.entity.DepartmentEntity;
import edu.vt.crest.hr.services.DepartmentService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Serves as a RESTful endpoint for manipulating DepartmentEntity(s)
 */
@Stateless
@Path("/departments")
public class DepartmentResource {

    //Used to interact with DepartmentEntity(s)
    @Inject
    DepartmentService departmentService;

    /**
     * Create a Department
     *
     * @param department the DepartmentEntity to create
     * @return a Response containing the new DepartmentEntity OR Error Message with respective status code
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(DepartmentEntity department) {
        try {
            DepartmentEntity departmentEntity = departmentService.createDepartment(department);
            return Response.status(Response.Status.CREATED).entity(departmentEntity).build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    /**
     * Get a DepartmentEntity using the given id
     *
     * @param id of the DepartmentEntity to return
     * @return a Response containing the matching DepartmentEntity
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {

        try {
            return Response.ok().entity(departmentService.findById(id)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    /**
     * List all the DepartmentEntity
     *
     * @param startPosition the index of the first DepartmentEntity to return
     * @param maxResult     the maximum number of DepartmentEntity(s) to return
     *                      beyond the startPosition
     * @return a list of DepartmentEntity(s) OR Error Message with respective status code
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<DepartmentEntity> listAll(@QueryParam("start") Integer startPosition,
                                          @QueryParam("max") Integer maxResult) {
            return departmentService.listAll(startPosition, maxResult);
    }

    /**
     * Update a DepartmentEntity with given id
     *
     * @param id         the id of the DepartmentEntity to update
     * @param department the entity used to update
     * @return a Response containing the updated DepartmentEntity OR Error Message with respective status code
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, DepartmentEntity department) {
        try {
            department.setId(id);
            DepartmentEntity latestObject = departmentService.findById(id);
            // Checks if object with given id is returned from database
            if (latestObject == null) {
                // Return Not found response
                return ServerException.onError(Response.Status.NOT_FOUND, String.format("Department with id %d NOT FOUND", id));
            }
            // Set Version from latest object
            // Fix for OptimisticLockException
            department.setVersion(latestObject.getVersion());
            DepartmentEntity departmentEntity = departmentService.update(id, department);
            return Response.ok().entity(departmentEntity).build();
        } catch (Exception e) {
            e.printStackTrace();
            return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

}
