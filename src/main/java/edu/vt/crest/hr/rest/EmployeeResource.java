package edu.vt.crest.hr.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.vt.crest.hr.entity.EmployeeEntity;
import edu.vt.crest.hr.services.EmployeeService;

/**
 * Serves as a RESTful endpoint for manipulating EmployeeEntity(s)
 */
@Stateless
@Path("/employees")
public class EmployeeResource {

	//Used to interact with EmployeeEntity(s)
	@Inject
	EmployeeService employeeService;

	/**
	 * Create an EmployeeEntity
	 * @param employee the EmployeeEntity to create
	 * @return a Response containing the new EmployeeEntity OR Error Message with respective status code
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(EmployeeEntity employee) {
		try{
			EmployeeEntity employeeEntity = employeeService.createEmployee(employee);
			return Response.status(Response.Status.CREATED).entity(employeeEntity).build();
		}
		catch (Exception e){
			e.printStackTrace();
			return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}

	}

	/**
	 * Get an EmployeeEntity using the given id
	 * @param id of the EmployeeEntity to return
	 * @return a Response containing the matching EmployeeEntity
	 */
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		EmployeeEntity employeeEntity = employeeService.findById(id);
		try {
			return Response.ok().entity(employeeEntity).build();
		} catch (Exception e) {
			e.printStackTrace();
			return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}

	/**
	 * List all the EmployeeEntity(s)
	 * @param startPosition the index of the first EmployeeEntity to return
	 * @param maxResult the maximum number of EmployeeEntity(s) to return
	 *                  beyond the startPosition
	 * @return a list of EmployeeEntity(s)
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EmployeeEntity> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
			return employeeService.listAll(startPosition, maxResult);
	}

	/**
	 * Update an EmployeeEntity
	 * @param id the id of the EmployeeEntity to update
	 * @param employee the entity used to update
	 * @return a Response containing the updated EmployeeEntity OR Error Message with respective status code
	 */
	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Long id, EmployeeEntity employee) {
		try{
            employee.setId(id);
            EmployeeEntity latestObject = employeeService.findById(id);
			// Checks if object with given id is returned from database
			if (latestObject == null) {
				// Return Not found response
				return ServerException.onError(Response.Status.NOT_FOUND, String.format("Department with id %d NOT FOUND", id));
			}
			// Set Version from latest object
			// Fix for OptimisticLockException
            employee.setVersion(latestObject.getVersion());
            EmployeeEntity employeeEntity = employeeService.update(id, employee);
            return Response.ok().entity(employeeEntity).build();
		}
		catch (Exception e){
			e.printStackTrace();
			return ServerException.onError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
}
