package edu.vt.crest.hr.rest;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class RestApplication extends Application {

}