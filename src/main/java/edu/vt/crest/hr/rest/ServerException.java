package edu.vt.crest.hr.rest;

import javax.ws.rs.core.Response;

/**
 * Exception Handler Class which include following properties:
 * <ul>
 *    <li>Handles error</li>
 *    <li>Returns Error Response</li>
 *    <li>Util Class</li>
 * <ul/>
 *
 * <p>
 *    TODO: onError can have other error handling logic
 * </p>
 */
public class ServerException{

    private ServerException(){}

    /**
     * Handle Error and return response with message
     * @param status error status
     * @param message error message
     * @return response with passed message
     */
    public static Response onError(Response.Status status, String message){
        return Response.status(status).entity(message).build();
    }
}
