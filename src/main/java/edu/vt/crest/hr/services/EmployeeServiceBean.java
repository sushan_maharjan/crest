package edu.vt.crest.hr.services;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.*;

import edu.vt.crest.hr.entity.DepartmentEntity;
import edu.vt.crest.hr.entity.EmployeeEntity;

/**
 * Implements an EmployeeService
 */
@ApplicationScoped
public class EmployeeServiceBean implements EmployeeService {

    @PersistenceContext
    private EntityManager em;

    /**
     * Persist an Employee in database
     *
     * @param employee the EmployeeEntity to create
     * @return EmployeeEntity to be persisted
     * <p>
     * save() of Hibernate Session can be used which returns the id of the persisted object,
     * and then return the persisted object using findById()
     * </p>
     */
    @Override
    public EmployeeEntity createEmployee(EmployeeEntity employee) {
        em.persist(employee);
        return employee;
    }

    /**
     * Find a single Employee using id
     *
     * @param id of the EmployeeEntity to return
     * @return the EmployeeEntity matching the id
     */
    @Override
    public EmployeeEntity findById(Long id) {
        return em.find(EmployeeEntity.class, id);
    }

    /**
     * List all the employees from the database
     *
     * @param startPosition the index of the first EmployeeEntity to return
     * @param maxResult     the maximum number of EmployeeEntity(s) to return
     *                      beyond the startPosition
     * @return List of all EmployeeEntity
     */
    @Override
    public List<EmployeeEntity> listAll(Integer startPosition, Integer maxResult) {
        TypedQuery typedQuery = em.createQuery("select e from Employee e", EmployeeEntity.class);
        //paginated only if the passed offset and limit are not null
        if (startPosition != null) {
            typedQuery.setFirstResult(startPosition);
        }
        if (maxResult != null)
            typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }

    /**
     * Update the employee in the database
     *
     * @param id       the id of the EmployeeEntity to update
     * @param employee the entity used to update
     * @return updated EmployeeEntity
     * @throws OptimisticLockException
     */
    @Override
    public EmployeeEntity update(Long id, EmployeeEntity employee) throws OptimisticLockException {
        return em.merge(employee);
    }
}
