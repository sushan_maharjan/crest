package edu.vt.crest.hr.services;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.vt.crest.hr.entity.DepartmentEntity;
import edu.vt.crest.hr.entity.EmployeeEntity;

/**
 * Implements a DepartmentService
 */
@ApplicationScoped
public class DepartmentServiceBean implements DepartmentService {

    @PersistenceContext
    private EntityManager em;


    @Inject
    DepartmentService departmentService;

    /**
     * Persist Department to database
     *
     * @param department the DepartmentEntity to create
     * @return departmentEntity to be persisted
     * <p>
     * Hibernate Session's save() method can be used instead of persist() which returns the id of the persisted DepartmentEntity, and then
     * </p>
     */
    @Override
    public DepartmentEntity createDepartment(DepartmentEntity department) {
        em.persist(department);
        return department;
    }

    /**
     * Find a single Department using id
     *
     * @param id of the DepartmentEntity to return
     * @return the DepartmentEntity using the given id
     */
    @Override
    public DepartmentEntity findById(Long id) {
        return em.find(DepartmentEntity.class, id);
    }

    /**
     * List all the Departments
     *
     * @param startPosition the index of the first DepartmentEntity to return
     * @param maxResult     the maximum number of DepartmentEntity(s) to return
     *                      beyond the startPosition
     * @return List of all Departments
     */
    @Override
    public List<DepartmentEntity> listAll(Integer startPosition, Integer maxResult) {
        TypedQuery typedQuery = em.createQuery("Select d from Department d order by d.id", DepartmentEntity.class);
        //paginated only if the passed offset and limit are not null
        if (startPosition != null) {
            typedQuery.setFirstResult(startPosition);
        }
        if (maxResult != null)
            typedQuery.setMaxResults(maxResult);
        return typedQuery.getResultList();
    }

    /**
     * Update the Department
     *
     * @param id         the id of the DepartmentEntity to update
     * @param department the entity used to update
     * @return DepartmantEntity
     * @throws OptimisticLockException
     */
    @Override
    public DepartmentEntity update(Long id, DepartmentEntity department) throws OptimisticLockException {
        return em.merge(department);
    }

}
