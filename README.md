<<<<<<< HEAD
CREST-HR is a Java EE 7 application that provides the skeleton for a simple 
RESTful API implementing a HR system. The application's API is deployed to a 
Wildfly 10 Java EE container that uses a PostgreSQL database to persist data.

The frontend is served by a node server. Browser requests are forwarded
to the backend API using a reverse-proxy managed by traefik 
(https://github.com/containous/traefik).
 
To get the system up and running quickly you should first download and 
install Docker (http://www.docker.com) if it is not already installed on 
your system. Once Docker is installed do the following:

1. Open a command line terminal and navigate to the project folder: 

``` cd crest-hr```

2. Enter the command: 

```docker-compose up```

This will download all application dependencies, configure the system, and 
start the application. As the system starts, it will 
automatically populate the CREST-HR database schema using the SQL located 
at $PROJECT_HOME/resources/META-INF/import.sql. Once up and running the 
application can be accessed at http://localhost/.  To access the API, 
go to http://localhost/api/.

The frontend has been designed to allow you to add/modify departments and
employees. Your task is to fill out the stubs in the backend API to 
implement the following functionality.

The API should allow a user to:

1. Create a new department
2. Update an existing department
3. Create a new employee
4. Update an existing employee

Note that you do not have to implement the ability to delete data.

There are multiple ways that you can deploy your WAR as your are testing
your application. The instructions that follow document ONE way to do this.

1. Using your browser access the Wildfly management console at
http://localhost:9990.
2. Username and password are admin/admin.
3. Click Deployments and then click Add in the upper left-hand corner.
4. Make sure Upload a new deployment is selected and click Next.
5. Select the WAR file that you want to deploy and click Next.
6. Click Finish. You should see a successful deployment message.

If all goes as expected you should now be able to access the frontend
by pointing your browser at http://localhost. For subsequent deployments
click on your WAR file in the management console and click Replace.


=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
>>>>>>> 08c04ecb2ed759ecb524c6173557afd29c9b38d7
